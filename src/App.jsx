/* eslint-disable react/prop-types */
import { useState } from "react";

function Square({ value, onSqureClick }) {
  return (
    <button className="square" onClick={onSqureClick}>
      {value}
    </button>
  );
}

function Board({ xIsNext, squares, onPlay }) {
  function handleClick(i) {
    //jika kotak sudah ada isi, maka jangan diisi atau sudah ada pemenangnya maka program berhenti
    if (squares[i] || calculateWinner(squares)) return;

    const nextSquares = squares.slice(); // membuat array baru / lifting state up

    nextSquares[i] = xIsNext ? "X" : "O";

    onPlay(nextSquares);
  }

  //menampilkan pemenang
  const winner = calculateWinner(squares);
  let status = "";
  if (winner) {
    status = "Winner: " + winner;
  } else {
    status = "Next player: " + (xIsNext ? "X" : "O");
  }

  return (
    <>
      <div className="status">{status}</div>
      <div className="board">
        <Square value={squares[0]} onSqureClick={() => handleClick(0)} />
        <Square value={squares[1]} onSqureClick={() => handleClick(1)} />
        <Square value={squares[2]} onSqureClick={() => handleClick(2)} />
        <Square value={squares[3]} onSqureClick={() => handleClick(3)} />
        <Square value={squares[4]} onSqureClick={() => handleClick(4)} />
        <Square value={squares[5]} onSqureClick={() => handleClick(5)} />
        <Square value={squares[6]} onSqureClick={() => handleClick(6)} />
        <Square value={squares[7]} onSqureClick={() => handleClick(7)} />
        <Square value={squares[8]} onSqureClick={() => handleClick(8)} />
      </div>
    </>
  );
}

export default function Game() {
  const [history, setHitory] = useState([Array(9).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0);
  const xIsNext = currentMove % 2 === 0;
  const currentSquares = history[currentMove];

  function jumpTo(nextMove) {
    setCurrentMove(nextMove);
  }

  function handlePlay(nextSquares) {
    const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
    setHitory(nextHistory);
    setCurrentMove(nextHistory.length - 1);
  }

  //tombol history
  const moves = history.map((squares, move) => {
    let description = "";
    if (move > 0) {
      description = "Go to move #" + move;
    } else {
      description = "Go to game start";
    }

    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>{description}</button>
      </li>
    );
  });

  return (
    <div className="game">
      <div className="game-board">
        <Board xIsNext={xIsNext} squares={currentSquares} onPlay={handlePlay} />
      </div>
      <div className="game-info">
        <ol>{moves}</ol>
      </div>
    </div>
  );
}

//menentukan aturan pemenang
function calculateWinner(squares) {
  //aturan garis pemenang
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8], //pemenang garis horizontal
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8], //pemenang garis vertikal
    [0, 4, 8],
    [2, 4, 6], //pemenang garis diagonal
  ];

  for (let i = 0; i < lines.length; i++) {
    // const a = lines[i][0]; // 0
    // const b = lines[i][1]; // 1
    // const c = lines[i][2]; // 2

    //destructoring
    const [a, b, c] = lines[i];

    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }

  return false;
}
